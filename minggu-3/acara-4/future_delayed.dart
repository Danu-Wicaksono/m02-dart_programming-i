Future delaPrint(int seconds, String message) {
  final durasi = Duration(seconds: seconds);
  return Future.delayed(durasi).then((value) => message);
}

main(List<String> args) {
  print("luffy");
  delaPrint(2, "Raja Bajak Laut").then((status) {
    print(status);
  });
  print("adalah");
}
