String orderMessage() {
  var order = userOrder();
  return 'Your order is : ${order}';
}

Future<String> userOrder() => Future.delayed(
      Duration(seconds: 3),
      () => 'Large Latte',
    );
void main(List<String> args) {
  print("Fetching user order....");
  print(orderMessage());
}
