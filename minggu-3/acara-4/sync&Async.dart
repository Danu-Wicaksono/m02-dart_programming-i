void main(List<String> args) async {
  var t = Titan();
  print("Zeke");
  print(t.name);
  await t.getName();
  print(t.name);
  print("Rainer");
}

class Titan {
  String name = "Eren Yaeger";
  Future<void> getName() async {
    await Future.delayed(Duration(seconds: 4));
    name = "Grisha";
    print("Getn Name [Done]");
  }
}
